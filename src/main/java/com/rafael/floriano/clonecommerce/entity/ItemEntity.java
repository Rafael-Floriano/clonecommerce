package com.rafael.floriano.clonecommerce.entity;

import jakarta.persistence.Entity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
public class ItemEntity {

    private Long idItem;

    private String description;

    private Double buyPrice;

    private Double sellPrice;

}
