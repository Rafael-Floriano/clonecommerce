package com.rafael.floriano.clonecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClonecommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClonecommerceApplication.class, args);
	}

}
